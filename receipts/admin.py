from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt
# Register your models here.
@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "owner",
    ]
    search_fields = ("name",)

@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "number",
        "owner",
    ]
    search_fields = ("name",)

@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = [
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
    ]
    search_fields=[
        "vendor",
        "purchaser_username",
        "category_name",
        "account_name",
    ]
    list_filter = [
        "category",
        "account",
        "date",
    ]
    date_hierarchy = "date"
