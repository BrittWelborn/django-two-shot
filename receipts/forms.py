
from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


class AccountForm(ModelForm):

    class Meta:
        model = Account
        fields = ["name", "number"]

class ExpenseCategoryForm(ModelForm):

    class Meta:
        model = ExpenseCategory
        fields = ['name']


class ReceiptForm(ModelForm):
    # email_address = forms.EmailField(max_length=300)

    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account"
        ]
